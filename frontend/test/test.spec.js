import Demo from "@/utils/test.js";
describe("Demo File", () => {
  it("TimeoutFn", done => {
    let cb = t => {
      expect(t).toBe("hello Timeout");
      done();
    }
    Demo.TimeoutFn(cb);
  })
  
  it("promiseFn", () => {
    return Demo.promiseFn().then(res => {
      expect(res).toBe("hello Promise")
    })
  })
  it("TimeoutAsnycFn", async () => {
    let res = await Demo.TimeoutAsnycFn();
    expect(res).toBe("hello asnyc");
  })
})