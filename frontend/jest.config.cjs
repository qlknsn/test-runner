module.exports = {
  // 预设
  preset: '@vue/cli-plugin-unit-jest',  
  // // 多于一个测试文件运行时展示每个测试用例测试通过情况
  verbose: true,
  // // 参数指定只要有一个测试用例没有通过，就停止执行后面的测试用例
  bail: true,
  // // 测试环境，jsdom 可以在 Node 虚拟浏览器环境运行测试
  testEnvironment: 'jsdom',
  // // 需要检测的文件类型(不需要配置)
  moduleFileExtensions: ['js', 'jsx', 'json', 'vue'],
  // // 预处理器配置，匹配的文件要经过转译才能被识别，否则会报错(不需要配置)
  // transform: {
  //   // 用 `vue-jest` 处理 `*.vue` 文件
  //   // ".*\\.(vue)$": "<rootDir>/node_modules/vue-jest",
  //   // 用 `babel-jest` 处理 js
  //   "^.+\\.js$": "babel-jest"
  // },
  // // 转译时忽略 node_modules
  // transformIgnorePatterns: ['/node_modules/'],
  // // 从正则表达式到模块名称的映射，和webpack的alisa类似(不需要配置)
  // moduleNameMapper: {
  //   '^@/(.*)$': '<rootDir>/src/$1'
  // },
  // // Jest用于检测测试的文件，可以用正则去匹配
  testMatch: [
    '**/tests/unit/**/*.spec.[jt]s?(x)',
  ],
  // // 是否显示覆盖率报告，开启后显示代码覆盖率详细信息，将测试用例结果输出到终端
  collectCoverage: true,
  // // // 告诉 jest 哪些文件需要经过单元测试测试
  collectCoverageFrom: ["src/assets/js/*.{js,vue}"],
  // // // 覆盖率报告输出的目录
  coverageDirectory: 'tests/unit/coverage',
  // // // 报告的格式
  // coverageReporters: ["html", "text-summary"],
  // // // 需要跳过覆盖率信息收集的文件目录
  coveragePathIgnorePatterns: ['/node_modules/'],
  // // 设置单元测试覆盖率阈值, 如果未达到阈值，Jest 将返回失败
  coverageThreshold: {
    global: {
      statements: 90, // 保证每个语句都执行了
      functions: 90, // 保证每个函数都调用了
      branches: 90, // 保证每个 if 等分支代码都执行了
      lines: 90
    },
  }
}