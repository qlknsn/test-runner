export default class Demo {
    static TimeoutFn (cb) {
      setTimeout(() => {
        cb && cb("hello Timeout");
      }, 1000)
    }
    static promiseFn () {
      return new Promise(r => {
        setTimeout(() => {
          r("hello Promise");
        }, 1000)
      })
    }
    static TimeoutAsnycFn () {
      return new Promise(r => {
        setTimeout(() => {
          r("hello asnyc");
        }, 1000)
      })
    }
}